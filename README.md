#P2CPSerial

**Problem:** automating testing of a CircuitPython board attached to a computer

**Solution:** create a serial connection from the computer to the board to send commands and receive responses

NOTE: this is a sample intended to be used as starter code for automating testing

## How It works

A circuitPython board can be in one of three possible states.
    1. Running - the board is running the code.py file
       Unfortunately, this could be at any point in the code execution
       so we need to get back t oa starting point
    2. REPL - the board is at the REPL prompt
    3. Reload - the board has stopped and will transition to another state

The goal is to get to either state #2 or #3 because then it is easy to 
transition to state #1. The code uses combinations of CTRL-B, CTRL-C, and
CTRL-D to get to a known state and then to the running state.

The only variable for any given board application is knowing when the
board is ready to receive commands. This detection is why we need the
the application response string.

Once the CircuitPython board is in the running state, use send_command()
and send_commands() methods to send commands to the board.
The send_command() function will send a single command and return the
'state' of the board. The send_commands() function will send a list of
commands to the board.

The 'commands' list is a list of commands to send to the board.
The list may contain strings, bytes, or integers:
    - strings = send the string (followed by a CRLF)
    - bytes = send the bytes (followed by a CRLF)
    - integers = sleep for the number of seconds
    
NOTE: In this example, the application response string is case sensitive.

## Setup

There are three pieces of information needed to use this sample:
    1. serial port of the CircuitPython board
    2. baud rate of the CircuitPython board
    3. application response string to indicate CircuitPython code is running

The application string could be anything and does not need to be the full
response from the CircuitPython board. It is used to determine when the
CircuitPython code is running and the board is ready to receive commands.

## Example:
``` Python
# copy or import of the P2CPSerial class and its dependencies
PAUSE = 5
commands = ['cmd1', 'cmd2', PAUSE, 'cmd3', 'quit']
tester = P2CPSerial('/dev/ttyUSB0', 115200, application_response'READY')
tester.begin()
tester.send_commands(commands)
```

## Prerequisites

PREREQUISITES: pip3 install pyserial

## Suggestions

There are many possible extensions to this sample.
- copying a test version of `code.py` prior to connecting
- branching command lists based of response data
- ability to start test on a CircuitPython board which is already running
- at REPL, send CircuitPython code and then begin test (avoid overwriting `code.py` when testing)
