""" ***************************************************************************
* File:    p2cpserial.py
* Date:    2024.05.28
* Author:  Bradán Lane STUDIO
*
* This content may be redistributed and/or modified as outlined
* under the MIT License
*
* ******************************************************************************/

--------------------------------------------------------------------------

a sample for programmatically communicating with a CircuitPython board via serial

This sample is intended as teh starting point for building automated tests
of a CircuitPython board. It is not intended to be a complete solution.

There are three pieces of information needed to use this sample:
    1. serial port of the CircuitPython board
    2. baud rate of the CircuitPython board
    3. application response string to indicate CircuitPython code is running

The application string could be anything and does not need to be the full
response from the CircuitPython board. It is used to determine when the
CircuitPython code is running and the board is ready to receive commands.

How this code works:

A circuitPython board can be in one of three possible states.
    1. Running - the board is running the code.py file
       Unfortunately, this could be at any point in the code execution
       so we need to get back t oa starting point
    2. REPL - the board is at the REPL prompt
    3. Reload - the board has stopped and will transition to another state

The goal is to get to either state #2 or #3 because then it is easy to 
transition to state #1. The code uses combinations of CTRL-B, CTRL-C, and
CTRL-D to get to a known state and then to the running state.

The only variable for any given board application is knowing when the
board is ready to receive commands. This detection is why we need the
the application response string.

Once the CircuitPython board is in the running state, use send_command()
and send_commands() functions to send commands to the board.
The send_command() function will send a single command and return the
'state' of the board. The send_commands() function will send a list of
commands to the board.

The 'commands' list is a list of commands to send to the board.
The list may contain strings, bytes, or integers:
    - strings = send the string (followed by a CRLF)
    - bytes = send the bytes (followed by a CRLF)
    - integers = sleep for the number of seconds
    
NOTE: In this example, the application response string is case sensitive.

PREREQUISITES: pip3 install pyserial

--------------------------------------------------------------------------
"""

from typing import SupportsIndex
import serial
import time

''' Define the serial port and baud rate '''
serial_device = '/dev/tty.usbmodem14401'    # MACOS
#serial_device = '/dev/ttyACM0'             # Linux
#serial_device = 'COM4'                     # Windows
serial_baud = 115200

''' Define the application response string '''
APPLICATION_RESPONSE = 'ENTER/RETURN'

''' add commands to teh following list '''
commands = []
commands = ['', '', 'take lantern', 'f', 'use lantern', 'r', 'l', 'i', 'look', 15, 'f', 'take towel', 'b', 15, 'q']


class P2CPSerial:
    ''' P2CPSerial is a class for communicating with a CircuitPython board via serial '''
    CTRLB = b'\x02'
    CTRLC = b'\x03'
    CTRLD = b'\x04'
    NEWLINE = b'\n\r'

    STATE_CONTINUE = 0
    STATE_RUNNING = 1
    STATE_REPL = 2
    STATE_RELOAD = 3
    STATE_NODATA = 4

    def __init__(self, serial_device, serial_baud=115200, application_response='press any key', timeout=2.0):
        self._ser = serial.Serial(serial_device, serial_baud, timeout=timeout)
        self._app_detection = application_response
        return
    def __del__(self):
        try:
            self._ser.close()
        except Exception as e:
            print(f"Error closing serial port: {e}")
        pass
    
    def begin(self):
        ''' get board into a known state and running default program (aka code.py) '''
        ready = False
        next_command = P2CPSerial.CTRLC
        response = 0
        print("[Re]Initializing CircuitPython board")
        while not ready:
            response = self.send_command(next_command, echo=False)
            if response == P2CPSerial.STATE_RUNNING:
                #print("CircuitPython code is running")
                ready = True
            elif response == P2CPSerial.STATE_REPL:
                print("REPL to Running")
                next_command = P2CPSerial.CTRLD
                continue
            elif response == P2CPSerial.STATE_RELOAD:
                print("Reload to Running")
                next_command = P2CPSerial.CTRLD
            else:
                print("Unknown to REPL")
                next_command = P2CPSerial.CTRLB
                #ready = True
        print('\n' * 8) # add blank lines before sending commands
        return ready
    
    def send_command(self, command, echo=True):
        if type(command) == int:
            time.sleep(command)
            return P2CPSerial.STATE_CONTINUE
        
        try:
            if type(command) == str:
                if echo:
                    print(command)
                command = command.encode() + P2CPSerial.NEWLINE
            else:
                if echo:
                    print(f"> '{command}'")
            self._ser.write(command)
        except Exception as e:
            print(f"Error {e} with command: '{command}'")
            return P2CPSerial.STATE_CONTINUE
        
        
        time.sleep(0.5) # short pause for board to respond
        
        # PySerial has a fixed buffer; the code loops to read all data
        response = ''
        # we wait up to 5 * 0.5 seconds for response
        for i in range(5):
            if self._ser.in_waiting:
                data = self._ser.read_all().decode().strip()
                if self._app_detection in data:
                    print(data)
                    return P2CPSerial.STATE_RUNNING
                
                if 'CircuitPython' in data:
                    return P2CPSerial.STATE_REPL
                
                if 'Use CTRL-D to reload' in data:
                    return P2CPSerial.STATE_RELOAD

                response += data
                time.sleep(0.25)
                # fi no more data, then return
                if self._ser.in_waiting == 0:
                    print(response)
                    return P2CPSerial.STATE_CONTINUE
            else:
                time.sleep(0.5)
        return P2CPSerial.STATE_NODATA

    def send_commands(self, commands, echo=True):
        for command in commands:
            response = self.send_command(command, echo)
            # if we crash, we stop
            if response == P2CPSerial.STATE_REPL:
                return
        return


tester = P2CPSerial(serial_device, serial_baud, APPLICATION_RESPONSE)
tester.begin()
if commands and len(commands) > 0:
    tester.send_commands(commands)
else:
    print("no commands list created")
